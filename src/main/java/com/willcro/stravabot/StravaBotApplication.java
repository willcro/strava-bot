package com.willcro.stravabot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StravaBotApplication {

  public static void main(String[] args) {
    SpringApplication.run(StravaBotApplication.class, args);
  }

}
